<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    private $htmlSlect;
    public function __construct()
    {
        $this->htmlSlect = '';
    }

    public function index()
    {
        return view('admin.page.category.index');
    }

    public function create()
    {

        $htmlOption = $this->categoryRecusive(0);

        return view('admin.page.category.add', compact('htmlOption'));
    }

    function categoryRecusive($id, $text = ''){
        $data = Category::all();

        foreach ($data as $value) {
            if ($value['parent_id'] == $id) {
                $this->htmlSlect .= "<option>" . $text . ' ' . $value['name'] . "</option>";
                $this->categoryRecusive($value['id'], $text. "一");
            }
        }

        return $this->htmlSlect;
    }
}
